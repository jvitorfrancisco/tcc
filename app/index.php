<?php
session_start();
$_SESSION['id'] = 2;
require_once 'config.php';
function __autoload($class_name){
    if (file_exists('./classes/'.$class_name.'.php')){
        require_once './classes/'.$class_name.'.php';
    }
    else if (file_exists('./controllers/'.$class_name.'.php')){
        require_once './controllers/'.$class_name.'.php';
    }
}