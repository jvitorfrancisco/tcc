<?php

class Controller{
    public static function loadTemplate($viewName){
        $view = $viewName;
        require_once './views/Template.php';
    }
    public static function loadView($viewName){
        require_once ('./views/'.$viewName.'.php');
    }
}