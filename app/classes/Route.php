<?php

class Route{
    public static $validRoutes = array();

    public static function set($route, $function){

        $verficar = login::Verificar();

        if ($verficar == false){
            login::loadTemplate('login');
        }

        self::$validRoutes[] = $route;

        if ($_GET['url'] == $route){
            $function->__invoke();
        }
    }
}